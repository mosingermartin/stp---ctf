#!/usr/bin/env bash
x=$(cat ~/vysledky/ukol1|sha512sum)
y=$(cat ~/vysledky/ukol2|sha512sum)
flag1="d70d4a8d0f739fa09af1c8c5d0fbd157eb843dc27697bc3d83a5892ae7169402a1e0633c2b6395835c375eb31a123bcf2125d51a293c073d1e5b3ab7beb712d4  -"
flag2="d70d4a8d0f739fa09af1c8c5d0fbd157eb843dc27697bc3d83a5892ae7169402a1e0633c2b6395835c375eb31a123bcf2125d51a293c073d1e5b3ab7beb712d4  -"

if [ "$flag1" == "$x" ]
then
	echo "ukol1 splnen" 
fi

if [ "$flag2" == "$y" ]
then
	echo "ukol2 splnen" 
fi
