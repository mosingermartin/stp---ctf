FROM ubuntu

COPY setup /root/setup



RUN apt-get update -y
RUN apt-get install -y gcc
RUN apt-get install -y python
RUN apt-get install -y curl
RUN apt-get install -y dnsutils
RUN apt-get install -y iputils-ping
RUN apt-get install -y net-tools

RUN apt-get install -y openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:malakrevetka' |chpasswd
RUN sed -ri 's/^#?PermitRootLogin\s+.*/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config
RUN mkdir /root/.ssh
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


EXPOSE 22

CMD    ["/usr/sbin/sshd", "-D"]



